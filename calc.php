<?php
/**
 * Calculator page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Calcular presupuesto - Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com/calc/">
    <script language="JavaScript" type="text/javascript">
      // Function calculating the total sum
      function calcResult() {
      	// Question 1 value
      	var val1 = parseInt($("#question1 :selected").val());
      	// If the basic website option is chosen
      	if (val1 === 165) {
      		// Set the total sum to 165
      		$("#total").text("El precio: €" + val1.toString() + " *");
      		// Hide the questions 2-4
      		$("#question2-group").hide();
      		$("#question3-group").hide();
      		// Show the #asterisk label
      		$("#asterisk").show();
      		return
      	} else {
      		// Else show the questions 2-4
      		$("#question2-group").show();
      		$("#question3-group").show();
      		// Show the #asterisk label
      		$("#asterisk").show();
      	}
      	// Questions 2-3 values
      	var val2 = parseFloat($("#question2 :selected").val());
      	var val3 = parseFloat($("#question3 :selected").val());
      	// Total sum
      	var valTotal = (val1 + val2 + val3) * 20;
      	// Writing the total sum to #total
      	$("#total").text("Su precio: €" + valTotal.toString() + " *");
      }
      
      // Calculate the result after loading the page
      window.onload = calcResult;
    </script>
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div id="contact" class="jumbotron jumbotron-fluid jumbotron-rounded bg-light-grey text-dark shadow">
        <div class="container">
          <h3 class="text-center py-1"><strong>¡Calcule su presupuesto!</strong></h3>
          <form id="calcform" name="calcform" action="#" method="post" role="form">
            <div id="question1-group" class="form-group">
              <label for="question1">Tipo de web</label>
              <select id="question1" name="question1" class="form-control custom-select" onchange="calcResult()">
                <option selected value="15">Web corporativa (representa a la empresa, negocio o marca)</option>
                <option value="24">Web profesional (incluye un catálogo de productos o servicios)</option>
                <option value="165">Web de presentación (logo, imagen, información de contacto)</option>
                <option value="34">Comercio electrónico (tienda online)</option>
              </select>
            </div>
            <div id="question2-group" class="form-group">
              <label for="question2">Redacción de textos</label>
              <select id="question2" name="question2" class="form-control custom-select" onchange="calcResult()">
                <option selected value="0">Textos proporcionados por el cliente</option>
                <option value="6">Textos redactados por Wildions</option>
              </select>
            </div>
            <div id="question3-group" class="form-group">
              <label for="question3">Fotografía y retoque fotográfico</label>
              <select id="question3" name="question3" class="form-control custom-select" onchange="calcResult()">
                <option selected value="0">Materiales proporcionados por el cliente</option>
                <option value="6">Materiales preparados por Wildions</option>
              </select>
            </div>
            <label id="total"></label>
            <label id="asterisk">
              * El precio no incluye IVA.
              <br />Además existen muchos factores adicionales que pueden afectar al precio de su web (cantidad de páginas, contenido etc). Para consultar el precio final ajustado a sus necesidades, por favor, <a href="contact.php">póngase en contacto con nosotros</a>.
            </label>
          </form>          
<?php echo file_get_contents(__DIR__ . "/html/return.html"); ?>  
        </div>
      </div>      
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>