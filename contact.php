<?php
/**
 * Contact page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */

// Load reCAPTCHA library
require_once __DIR__ . "/recaptchalib.php";
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Contacto - Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com/contact/">
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div id="contact" class="jumbotron jumbotron-fluid jumbotron-rounded bg-light-grey text-dark shadow">
        <div class="container">
          <address>
            <p class="indented">Puede contactar con nosotros llamando a nuestro móvil, por correo electrónico o rellenando el formulario:</p>
            <a href="tel:+34644712878"><img class="icon mr-2" src="img/icons/device-mobile.svg" alt="Teléfono 1" title="Teléfono 1">+34 644 71 28 78</a><br />
            <a href="tel:+34644632779"><img class="icon mr-2" src="img/icons/device-mobile.svg" alt="Teléfono 2" title="Teléfono 2">+34 644 63 32 79</a><br />
            <a href="mailto:info@wildions.com"><img class="icon mr-2" src="img/icons/mail.svg" alt="Email" title="Email">info@wildions.com</a>
          </address>
          <form id="contact-form" method="post" action="contactscript.php" role="form">
            <div class="emailStatus"></div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="emailFirstName">Nombre</label>
                <input class="form-control" id="emailFirstName" name="firstName" required="required" data-error="Este campo es obligatorio.">
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-md-6">
                 <label for="emailLastName">Apellido(s)</label>
                 <input class="form-control" id="emailLastName" name="lastName" required="required" data-error="Este campo es obligatorio.">
                 <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="emailAddress">Correo electrónico</label>
                <input type="email" class="form-control" id="emailAddress" name="email" required="required" data-error="Este campo es obligatorio.">
                <div class="help-block with-errors"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="emailPhone">Teléfono</label>
                <input type="tel" class="form-control" id="emailPhone" name="phone">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="emailSubject">Asunto</label>
                <input class="form-control" id="emailSubject" name="subject" required="required" data-error="Este campo es obligatorio.">
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="emailBody">Mensaje</label>
                <textarea class="form-control" id="emailBody" name="message" rows="4" required="required" data-error="Este campo es obligatorio."></textarea>
                <div class="help-block with-errors"></div>
              </div>
            </div>
            <div class="form-group form-check-inline">
              <input type="checkbox" class="form-check-input" id="emailCheck" required="required" data-error="Tiene que aceptar nuestra política de privacidad.">
              <label class="form-check-label" for="emailCheck">
                Acepto <a href="legal-notice.php#privacy">la política de privacidad</a>
              </label>
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group">
              <div class="g-recaptcha" data-sitekey="6LcVkHoUAAAAAAcxBpH1GZfiK5Ua5hjf_cZNXiJU"></div>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>       
          </form>
<?php echo file_get_contents(__DIR__ . "/html/return.html"); ?>  
        </div>
      </div>
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>
