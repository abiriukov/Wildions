<?php
/**
 * Contact form processing
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */

// Load reCAPTCHA library
require_once __DIR__ . "/recaptchalib.php";

// Secret key
$secret = "6LcVkHoUAAAAAKRe_AVj3Ycp4OSOGIX_enyjfuDI";
 
// Empty response
$response = null;
 
// New reCAPTCHA check
$reCaptcha = new ReCaptcha($secret);

// If reCAPTCHA is submitted, check the response
if ($_POST["g-recaptcha-response"]) {
    $response = $reCaptcha->verifyResponse(
        $_SERVER["REMOTE_ADDR"],
        $_POST["g-recaptcha-response"]
    );
}

// If the response is successful
if ($response == null || !($response->success)) {
    print "Error: Tiene que resolver la CAPTCHA."; 
    exit;
}
 
// Values from the form
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$subject = $_POST['subject'];
$tempMessage = $_POST['message'];

// Errors
$pattern = "#^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@";
$pattern .= "([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$#";

if ((!$email)
    || (strlen($_POST['email']) > 200)
    || (!preg_match(
        $pattern, $email
    ))
) {
    print "Error: La dirección email es incorrecta"; 
    exit; 
}

if (eregi("\r", $email) || eregi("\n", $email)) { 
    print "Error: La dirección email es incorrecta"; 
    exit; 
} 

// Preparing the email
$headers = "Content-Type: text/html; charset=utf-8\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Reply-To: ".$firstName." ".$lastName." <".$email.">\r\n";  
$recipient = "info@wildions.com";
$message = "<html>";
$message .= "<body>";
$message .= "<img src='https://www.wildions.com/img/wildions-logo.png' alt='Logo de Wildions' height='100'><hr>";
$message .= "<h1>Nuevo mensaje del formulario</h1><hr>";
$message .= "<p>Nombre: ".$firstName."</p>";
$message .= "<p>Apellido(s): ".$lastName."</p>";
$message .= "<p>Correo electrónico: ".$email."</p>";
$message .= "<p>Teléfono: ".$phone."</p>";
$message .= "<p>Asunto: ".$subject."</p>";
$tempMessage = wordwrap($tempMessage, 1024, "\n");
$message .= "<p>Mensaje:</p><p>".$tempMessage."</p>";
$message .= "</body></html>";

// Sending the email
mail($recipient, $subject, $message, $headers);

// Redirecting
header("location: mail-sent.php");
?>