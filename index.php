<?php
/**
 * Home page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com">
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div class="jumbotron jumbotron-fluid special-offer-banner bg-light-grey text-dark shadow">
        <div class="row align-items-center">
          <div class="col col-md-6">
            <h2 class="text-uppercase text-center">TARIFA PLANA EN MANTENIMIENTO DE SOFTWARE</h2>
            <p>Por sólo <strong>16,95 € / mes</strong> contrata mantenimiento de PC, que incluye:</p>
			     <ul class="fa-ul">
				    <li><i class="fa-li fa fa-check"></i>Borrado de archivos temporales, archivos basura, Malware y virus.</li>
				    <li><i class="fa-li fa fa-check"></i>Reparación de errores de registro, optimización de arranque y rendimiento.</li>
				    <li><i class="fa-li fa fa-check"></i>Revisión programas instalados.</li>
				  </ul>
          </div>
          <div class="col col-md-6">
            <h2 class="text-uppercase text-center">ASISTENCIA REMOTA</h2>
            <p>Contrata nuestro <strong>BONO</strong> de <strong>3 CONSULTAS</strong> por sólo <strong>16,95 €</strong>.</p>
          </div>
        </div>
        <h3 class="text-center">Envía un correo a <a href="mailto:info@wildions.com">info@wildions.com</a> o rellena el <a href="contact.php">FORMULARIO</a></h3>
        <!-- <h1 class="text-uppercase text-center">Oferta especial - Web básica</h1>
        <p>Por sólo <strong>€165</strong> le ofrecemos una web que será su <strong>tarjeta de presentación</strong> y contendrá:</p>
        <ul class="fa-ul">
		    <li><i class="fa-li fa fa-check"></i>El <strong>logotipo</strong> de su empresa</li>
		    <li><i class="fa-li fa fa-check"></i>Una <strong>descripción</strong> de productos o servicios que ofrece</li>
		    <li><i class="fa-li fa fa-check"></i>Una <strong>imagen</strong> o <strong>fotografía</strong> que representa su negocio</li>
		    <li><i class="fa-li fa fa-check"></i><strong>Información de contacto</strong> y el <strong>mapa</strong> incrustado</li>
		  </ul>
		  <a class="btn btn-dark-grey btn-lg" href="/calc/" role="button">Calcule su presupuesto</a> -->
      </div>
      <div id="chalkboard" class="container-fluid text-center">
        <div class="menu-item">
          <a href="/philosophy/">
            <picture>
	           <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="img/philosophy.webp" media="(min-width: 768px)" type="image/webp">
              <source srcset="img/philosophy.png" media="(min-width: 768px)">
              <source srcset="img/philosophy-576.webp" media="(min-width: 576px)" type="image/webp">
              <source srcset="img/philosophy-576.png" media="(min-width: 576px)">
              <source srcset="img/philosophy-480.webp" media="(min-width: 480px)" type="image/webp">
              <source srcset="img/philosophy-480.png" media="(min-width: 480px)">
              <source srcset="img/philosophy-240.webp" type="image/webp">
              <!--[if IE 9]></video><![endif]-->
              <img src="img/philosophy-240.png" alt="Nuestra filosofía" title="Nuestra filosofía">
	         </picture>
          </a>
        </div>
        <div class="menu-item">
          <a href="/services/">
            <picture>
	           <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="img/services.webp" media="(min-width: 768px)" type="image/webp">
              <source srcset="img/services.png" media="(min-width: 768px)">
              <source srcset="img/services-576.webp" media="(min-width: 576px)" type="image/webp">
              <source srcset="img/services-576.png" media="(min-width: 576px)">
              <source srcset="img/services-480.webp" media="(min-width: 480px)" type="image/webp">
              <source srcset="img/services-480.png" media="(min-width: 480px)">
              <source srcset="img/services-240.webp" type="image/webp">
              <!--[if IE 9]></video><![endif]-->
              <img src="img/services-240.png" alt="Nuestros servicios" title="Nuestros servicios">
	         </picture>
          </a>
        </div>
        <div class="menu-item">
          <a href="/contact/">
            <picture>
	           <!--[if IE 9]><video style="display: none;"><![endif]-->
              <source srcset="img/contact.webp" media="(min-width: 480px)" type="image/webp">
              <source srcset="img/contact.png" media="(min-width: 480px)">
              <source srcset="img/contact-240.webp" type="image/webp">
              <!--[if IE 9]></video><![endif]-->
              <img src="img/contact-240.png" alt="Contacto" title="Contacto">
	         </picture>
          </a>
        </div>
        <div class="works">
          <picture>
	         <!--[if IE 9]><video style="display: none;"><![endif]-->
	         <source srcset="img/works.webp" media="(min-width: 992px)" type="image/webp">
            <source srcset="img/works.png" media="(min-width: 992px)">
            <source srcset="img/works-768.webp" media="(min-width: 768px)" type="image/webp">
            <source srcset="img/works-768.png" media="(min-width: 768px)">
            <source srcset="img/works-576.webp" media="(min-width: 576px)" type="image/webp">
            <source srcset="img/works-576.png" media="(min-width: 576px)">
            <source srcset="img/works-480.webp" media="(min-width: 480px)" type="image/webp">
            <source srcset="img/works-480.png" media="(min-width: 480px)">
            <source srcset="img/works-240.webp" type="image/webp">
            <!--[if IE 9]></video><![endif]-->
            <img src="img/works-240.png" alt="Algunos de nuestros trabajos" title="Algunos de nuestros trabajos">
	       </picture>
        </div>
      </div>
      
      <div id="clients" class="jumbotron jumbotron-fluid jumbotron-rounded">
        <div class="container">
          <div class="row align-items-center justify-content-center mb-2">
            <div class="col-lg-4 col-md-5 col-sm-6 col-6 mx-auto my-auto bg-dark">
              <a href="https://ventturi.net" rel="nofollow">
                <picture>
	               <!--[if IE 9]><video style="display: none;"><![endif]-->
	               <source srcset="img/clients/ventturi.webp" media="(min-width: 480px)" type="image/webp">
                  <source srcset="img/clients/ventturi.png" media="(min-width: 480px)">
                  <source srcset="img/clients/ventturi-240.webp" type="image/webp">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="img/clients/ventturi-240.png" alt="Logo Ventturi" class="img-fluid" title="Logo Ventturi">
	             </picture>
              </a>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-6 mx-auto my-auto">
              <a href="https://autogamatallermecanico.com" rel="nofollow">
                <picture>
	               <!--[if IE 9]><video style="display: none;"><![endif]-->
	               <source srcset="img/clients/autogama.webp" media="(min-width: 576px)" type="image/webp">
                  <source srcset="img/clients/autogama.png" media="(min-width: 576px)">
                  <source srcset="img/clients/autogama-480.webp" media="(min-width: 480px)" type="image/webp">
                  <source srcset="img/clients/autogama-480.png" media="(min-width: 480px)">
                  <source srcset="img/clients/autogama-240.webp" type="image/webp">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="img/clients/autogama-240.png" alt="Logo Autogama" class="img-fluid" title="Logo Autogama">
	             </picture>
              </a>
            </div>
          </div>
          <div class="row align-items-center justify-content-center mb-2">
            <div class="col-lg-4 col-md-5 col-sm-6 col-6 mx-auto my-auto">
              <a href="https://administrativoporhoras.com" rel="nofollow">
                <picture>
	               <!--[if IE 9]><video style="display: none;"><![endif]-->
	               <source srcset="img/clients/maytb.webp" media="(min-width: 576px)" type="image/webp">
                  <source srcset="img/clients/maytb.png" media="(min-width: 576px)">
                  <source srcset="img/clients/maytb-480.webp" media="(min-width: 480px)" type="image/webp">
                  <source srcset="img/clients/maytb-480.png" media="(min-width: 480px)">
                  <source srcset="img/clients/maytb-240.webp" type="image/webp">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="img/clients/maytb-240.png" alt="Logo MayTB" class="img-fluid" title="Logo MayTB">
	             </picture>
              </a>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-6 mx-auto my-auto bg-dark">
              <a href="https://bodegasabinasa.com" rel="nofollow">
                <picture>
	               <!--[if IE 9]><video style="display: none;"><![endif]-->
	               <source srcset="img/clients/abinasa.webp" media="(min-width: 480px)" type="image/webp">
                  <source srcset="img/clients/abinasa.png" media="(min-width: 480px)">
                  <source srcset="img/clients/abinasa-240.webp" type="image/webp">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="img/clients/abinasa-240.png" alt="Logo Bodegas Abinasa" class="img-fluid" title="Logo Bodegas Abinasa">
	             </picture>
              </a>
            </div>
          </div>
          <div class="row align-items-center justify-content-center mb-2">
            <div class="col-lg-4 col-md-5 col-sm-6 col-6 mx-auto my-auto">
              <a href="https://tomyshop.ru" rel="nofollow">
                <picture>
	               <source srcset="img/clients/tomyshop.webp" media="(min-width: 992px)" type="image/webp">
		            <source srcset="img/clients/tomyshop.png" media="(min-width: 992px)">
		            <source srcset="img/clients/tomyshop-768.webp" media="(min-width: 768px)" type="image/webp">
		            <source srcset="img/clients/tomyshop-768.png" media="(min-width: 768px)">
	               <source srcset="img/clients/tomyshop-576.webp" media="(min-width: 576px)" type="image/webp">
                  <source srcset="img/clients/tomyshop-576.png" media="(min-width: 576px)">
                  <source srcset="img/clients/tomyshop-480.webp" media="(min-width: 480px)" type="image/webp">
                  <source srcset="img/clients/tomyshop-480.png" media="(min-width: 480px)">
                  <source srcset="img/clients/tomyshop-240.webp" type="image/webp">
                  <!--[if IE 9]></video><![endif]-->
                  <img src="img/clients/tomyshop-240.png" alt="Logo Tomyshop" class="img-fluid" title="Logo Tomyshop">
	             </picture>
              </a>
            </div>
          </div>          
        </div>
      </div>
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>