<?php
/**
 * Mail sent page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Mensaje enviado - Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com/mail-sent/">
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div id="contact" class="jumbotron jumbotron-fluid jumbotron-rounded bg-light-grey text-dark shadow">
        <div class="container">
          <h3 class="indented"><strong>¡Su mensaje ha sido enviado con éxito!</strong></h3>
          <p class="indented">Gracias por su consulta. Intentaremos responderle lo antes posible.</p>
<?php echo file_get_contents(__DIR__ . "/html/return.html"); ?>  
        </div>
      </div>
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>
