<?php
/**
 * Our philosophy page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Nuestra filosofía - Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com/philosophy/">
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div id="philosophy" class="jumbotron jumbotron-fluid jumbotron-rounded bg-light-grey text-dark shadow">
        <div class="container text-justify">
          <p class="indented"><strong>Wildions</strong> somos un equipo de profesionales decididos a dar un servicio de CREACIÓN WEB comprometido, personalizado, colaborativo y justo.</p>
          <p class="indented"><strong>Creación web</strong>, porque hacemos páginas web escribiendo código, sin usar plantillas predeterminadas. Esto nos da libertad total para realizar diseños originales y únicos.</p>
          <p class="slogan">"No nos gusta poner límites a las ideas."</p>
          <p class="indented"><strong>Nos comprometemos</strong> a cuidar el posicionamiento en buscadores, la presencia en redes sociales y todo lo necesario para que su web sea dinámica y productiva.</p>
          <p class="slogan">"Una web inacabada no es provechosa y perjudica la imagen de su empresa."</p>
          <p class="indented"><strong>Garantizamos</strong> que su web cumplirá con la <strong>legislación</strong> vigente. Miles de páginas incumplen las exigencias legales con desconocimiento de sus propietarios.</p>
          <p class="slogan">"No adjuntar correctamente los apartados legales puede ser motivo de sanción."</p>
          <p class="indented"><strong>Cada autónomo</strong> y cada empresa son <strong>únicos</strong>, con necesidades  diferentes. Algunas tarifas estándar no son justas para autónomos o pequeños negocios.</p>
          <p class="slogan">"Equilibrio entre precio y necesidades es posible."</p>
          <p class="indented"><strong>Si tiene</strong> muy <strong>claro</strong> el diseño de su página, le invitamos a participar para hacer realidad sus ideas. Si prefiere dejarlo en nuestras manos, nos ocupamos de todo (diseño de la web, logotipos, fotografías, textos etc)</p>
          <p class="indented"><strong>Nuestros clientes pueden elegir</strong> libremente el proveedor de <strong>hosting</strong> para su web. Del servicio de hosting depende el buen funcionamiento de la web y su seguridad, así como el nombre de dominio. La finalidad de la libre elección es que el cliente sea el propietario de su dominio, sin intermediarios. Que pueda elegir cuánto gastar y qué servicios recibir.</p>
          <p class="slogan">"Nosotros podemos asesorarle, pero nunca decidir por usted."</p>
          <p class="indented"><strong>No cobramos cuota</strong> de mantenimiento. Si el cliente solicita cambios en el contenido de la web, que dependen únicamente de nosotros, se le cobrará por el trabajo realizado.</p>
          <p class="slogan">"No cobramos por lo que no hacemos."</p>
<?php echo file_get_contents(__DIR__ . "/html/return.html"); ?>  
        </div>
      </div>
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>
