<?php
/**
 * Our services page
 *
 * PHP version 7
 *
 * @category  Web
 * @package   Wildions
 * @author    Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @copyright 2018-2020 Aleksandr Biriukov <wildions.programmer@gmail.com>
 * @license   https://www.wildions.com GPL
 * @link      https://www.wildions.com
 */
?>
<?php echo file_get_contents(__DIR__ . "/html/header1.html"); ?>
    <title>Nuestros servicios - Wildions - Creación Web - Servicios Informáticos Integrales</title>
    <link rel="canonical" href="https://wildions.com/services/">
<?php echo file_get_contents(__DIR__ . "/html/header2.html"); ?>
      <div id="services" class="jumbotron jumbotron-fluid jumbotron-rounded bg-light-grey text-dark shadow">
        <div class="container">
          <p class="indented text-justify">Además de la creación web (diseños, logotipos, fotografías, textos etc) ofrecemos algunos servicios adicionales como:</p>
          <div class="row py-2 bg-dark-grey text-light">
            <div class="col-6 my-auto">
              <span>Mantenimiento e instalación de equipos en general.</span>
            </div>
            <div class="col-6 my-auto">
              <img class="img-fluid rounded" src="img/computer-3293875_1920.webp" alt="Mantenimiento e instalación de equipos" title="Mantenimiento e instalación de equipos">
              </div>
          </div>
          <div class="row py-2 bg-light-grey text-dark">
            <div class="col-6 my-auto">
              <img class="img-fluid rounded" src="img/buy-3692440_1920.webp" alt="Asesoramiento en la compra de equipos re-acondicionados" title="Asesoramiento en la compra de equipos re-acondicionados">
            </div>
            <div class="col-6 my-auto">
              <span>Asesoramiento en la adquisición de equipos informáticos re-acondicionados de alta gama a precios económicos con total garantía de calidad.</span>
            </div>
          </div>
          <div class="row py-2 bg-dark-grey text-light">
            <div class="col-6 my-auto">
              <span>Asistencia técnica y reparaciones en fines de semana y festivos. Pensando en aquellos que destinan este tiempo al trabajo con el ordenador.</span> 
              <br />
              <span>En ocasiones surgen pequeños problemas. Algunos se pueden solucionar con asistencia remota, otros requieren asistencia presencial, pero si se pueden solventar no suponen un retraso de varios días.</span>
            </div>
            <div class="col-6 my-auto">
              <img class="img-fluid rounded" src="img/despaired-2261021_1920.webp" alt="Asistencia técnica en fines de semana y festivos" title="Asistencia técnica en fines de semana y festivos">
            </div>
          </div>
          <div class="row py-2 bg-light-grey text-dark">
            <div class="col-6 my-auto">
              <img class="img-fluid rounded" src="img/hard-drive-3094771_1920.webp" alt="Copias de seguridad" title="Copias de seguridad">
            </div>
            <div class="col-6 my-auto">
              <span>Copias de seguridad. Díganos cómo y con qué frecuencia quiere asegurar sus archivos. Nosotros nos ocupamos a distancia guardando su copia en la nube o presencial si además desea copias físicas.</span>
            </div>
          </div>
<?php echo file_get_contents(__DIR__ . "/html/return.html"); ?>  
        </div>
      </div>
<?php echo file_get_contents(__DIR__ . "/html/footer.html"); ?>
